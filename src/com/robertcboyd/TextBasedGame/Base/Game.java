package com.robertcboyd.TextBasedGame.Base;

//the main class which does very little
public class Game {
	//this is the global player variable
	//TODO player should not be a global variable
	private static Player p;

	/**
	 * The main method.
	 *
	 * @param args Not used.
	 */
	public static void main(String[] args)
	{
		Player p = new Player();
		p.setLocation(Room.getRoom1());
		while(!gameOver()) {
			p.getLocation().onEnter(p);
		}
	}

	/**
	 * Decides if the game is over.
	 *
	 * @return True if the game is over, false otherwise.
	 */
	public static boolean gameOver() {
		return false;
		//TODO for now
	}

	/**
	 * Gets the currently active Player.
	 *
	 * @return the active Player
	 */
	public static Player getPlayer()
	{
		return p;
	}

}
