package com.robertcboyd.TextBasedGame.Base;

import java.util.Vector;

public class Player {
	private int hp;
	private Room location;
	private Vector<Item> inventory;

	public Room getLocation()
	{
		return location;
	}
	
	public void setLocation(Room room)
	{
		this.location = room;
	}
}
