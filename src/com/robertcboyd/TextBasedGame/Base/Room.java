package com.robertcboyd.TextBasedGame.Base;

import com.robertcboyd.TextBasedGame.Commands.Command;
import com.robertcboyd.TextBasedGame.Parser.Sentence;
import com.robertcboyd.TextBasedGame.Parser.WordParser;

public class Room {
	private String name;
	private String description;

	/**
	 * Called when a Player enters the Room. Prints out the Room's description and loops while the Player remains in the Room.
	 *
	 * @param player entering Player
	 */
	public void onEnter(Player player)
	{
		this.printDescription();
		do {
			WordParser.processInput(UI.getInput());
		} while (player.getLocation().equals(this));
	}

	/**
	 * Prints the description of this Room to the console.
	 */
	public void printDescription()
	{
		if(!this.name.equals("")) {
			UI.println("|" + this.name + "|");
		}
		UI.println(this.getDescription());
	}

	public String getDescription()
	{
		return description;
	}

	/**
	 * Standard Room constructor.
	 *
	 * @param name Desired name for the new Room
	 * @param description Desired description for the new Room
	 *
	 * @return The newly constructed Room
	 */
	public Room(String name, String description)
	{
		this.name = name;
		this.description = description;
	}

	/**
	 * Room constructor for locations without a specific name.
	 *
	 * @param description Desired description for the new Room
	 *
	 * @return The newly constructed Room
	 */
	public Room(String description)
	{
		this.name = "";
		this.description = description;
	}

	//rooms
	private static Room room1 = new Room("Hill", "You walk up a long path to the top of a nearby hill. "
			+ "The air is clear and there is a castle far to the north.") {
		@Override
		public Room getPlaceNorthOf()
		{
			return getRoom2();
		}
	};
	
	public static Room getRoom1()
	{
		return room1;
	}

	private static Room room2 = new Room("You reach a grasslands area at the foot of a hill to the south. To the north there is a castle.") {
		@Override
		public Room getPlaceSouthOf()
		{
			return getRoom1();
		}
	};

	public static Room getRoom2()
	{
		return room2;
	}

	/**
	 * Gets the Room to the north of this Room. Returns null by default, must be overridden with a return statement with the appropriate Room (or, more likely, a lazy initialization method that returns the correct Room).
	 *
	 * @return Room to the north if one exists, otherwise null.
	 */
	public Room getPlaceNorthOf()
	{
		return null;
	}

	/**
	 * Gets the Room to the south of this Room. Returns null by default, must be overridden with a return statement with the appropriate Room (or, more likely, a lazy initialization method that returns the correct Room).
	 *
	 * @return Room to the south if one exists, otherwise null.
	 */
	public Room getPlaceSouthOf()
	{
		return null;
	}

	/**
	 * Gets the Room to the east of this Room. Returns null by default, must be overridden with a return statement with the appropriate Room (or, more likely, a lazy initialization method that returns the correct Room).
	 *
	 * @return Room to the east if one exists, otherwise null.
	 */
	public Room getPlaceEastOf()
	{
		return null;
	}

	/**
	 * Gets the Room to the west of this Room. Returns null by default, must be overridden with a return statement with the appropriate Room (or, more likely, a lazy initialization method that returns the correct Room).
	 *
	 * @return Room to the west if one exists, otherwise null.
	 */
	public Room getPlaceWestOf()
	{
		return null;
	}
}
