package com.robertcboyd.TextBasedGame.Base;

import java.util.Scanner;

public class UI {
	private static Scanner scanner = new Scanner(System.in);

	/**
	 * Asks the user for input in a nice way with a colon for a prompt.
	 *
	 * @return A String with the user's input.
	 */
	public static String getInput()
	{
		println();
		println("What would you like to do?");
		print(":");
		return scanner.nextLine();
	}
	
	/**
	 * Wraps System.out.print(). Should be used any time one wishes to print information to the user console in a manner like System.out.print().
	 *
	 * @param string The String to be printed.
	 */
	public static void print(String string)
	{
		System.out.print(string);
	}

	/**
	 * Wraps System.out.println(). Should be used any time one wishes to print information to the user console in a manner like System.out.println().
	 *
	 * @param string The String to be printed.
	 */
	public static void println(String string)
	{
		System.out.println(string);
	}

	/**
	 * Prints a blank line. Simply a shorthand for UI.println("").
	 */
	public static void println()
	{
		System.out.println("");
	}
	
}
