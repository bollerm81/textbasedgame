package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Parser.Sentence;

// a class to represent any action the player can take in the game

public interface Command {
	
	/**
	 * This method, when implemented, should do all of the appropriate internal work to reflect the action's consequences.
	 *
	 * @param tokenizedInput The Sentence representation of the user's input
	 */
	public void invoke(Sentence tokenizedInput);
}
