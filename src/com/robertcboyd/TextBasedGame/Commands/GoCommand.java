package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.Game;
import com.robertcboyd.TextBasedGame.Base.Room;
import com.robertcboyd.TextBasedGame.Base.UI;
import com.robertcboyd.TextBasedGame.Parser.Sentence;

// the quintissential movement command TODO: spelling

public class GoCommand implements Command{

	/**
	 * Causes the player's location to be set to the destination in accordance with the given cardinal direction or other specifier.
	 *
	 * @param tokenizedInput The Sentence representation of the user's input
	 */
	@Override
	public void invoke(Sentence tokenizedInput)
	{
		String locationAsString = tokenizedInput.getObject().toString();
		//null check to make sure that an object exists in the sentence
		if(locationAsString == null) {
			//TODO: error
		}
		Room destination;
		switch(locationAsString) {
			case "n":
			case "north":
				destination = Game.getPlayer().getLocation().getPlaceNorthOf();
				break;
			case "s":
			case "south":
				destination = Game.getPlayer().getLocation().getPlaceSouthOf();
				break;
			case "e":
			case "east":
				destination = Game.getPlayer().getLocation().getPlaceEastOf();
				break;
			case "w":
			case "west":
				destination = Game.getPlayer().getLocation().getPlaceWestOf();
				break;
			default:
				destination = null;
		}

		if(destination != null) {
			Game.getPlayer().setLocation(destination);
		} else {
			UI.println("'" + locationAsString + "' doesn't appear to be a place you can " + tokenizedInput.getVerb() + " to from here.");
		}
	}
}
