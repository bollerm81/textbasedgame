package com.robertcboyd.TextBasedGame.Commands;

import com.robertcboyd.TextBasedGame.Base.UI;
import com.robertcboyd.TextBasedGame.Parser.Sentence;

public class NoVerbCommand implements Command{

	/**
	 * Informs the user that no verb was detected in his/her sentence.
	 *
	 * @param tokenizedInput The user's input, unused in this Command
	 */
	@Override
	public void invoke(Sentence tokenizedInput)
	{
		UI.println("I don't see a verb in your sentence...");
	}
}
