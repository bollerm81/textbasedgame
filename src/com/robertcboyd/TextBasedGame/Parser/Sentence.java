package com.robertcboyd.TextBasedGame.Parser;

import java.util.Vector;

//a class to represent a sentence (the final parsed user input)
//WARNING: this currently only supports very basic, imperative sentence structure

public class Sentence {
	/*
	 * tokenizedRealInput should be simply the tokenized version of the user's input
     */

	private Vector<Word> tokenizedRealInput;

	//currently simply returns the first word if that word is in the dictionary
	/**
	 * Gets the verb for the Sentence. Currently simply selects the first word, as the first word is frequently the verb.
	 *
	 * @return The verb in the Sentence, null if none.
	 */
	public Word getVerb()
	{
		if (tokenizedRealInput != null) {
			return tokenizedRealInput.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Gets the object for the Sentence.
	 *
	 * @return The object in the Sentence, null if no object exists
	 */
	public Word getObject()
	{
		if (tokenizedRealInput != null) {
			return tokenizedRealInput.get(1);
		} else {
			return null;
		}
		//TODO: This implementation is very bare bones and relies on the most common case
	}

	/**
	 * Creates an array of all of the object's modifiers.
	 *
	 * @return The object in the Sentence, null if no such modifiers exist
	 */
	public Word[] getObjectModifiers()
	{
		if (tokenizedRealInput != null) {
			Word[] returnArray = new Word[tokenizedRealInput.size() - 2];
			for(int i = 2; i < tokenizedRealInput.size(); i++) {
				returnArray[i - 2] = tokenizedRealInput.get(i);
			}
			return returnArray;
		} else {
			return null;
		}
		//TODO: strengthen implementation
	}

	/**
	 * Constructs a sentence.
	 *
	 * @param tokenizedInput A Vector containing the tokenized words in the user's input, in order
	 *
	 * @return A newly constructed sentence
	 */
	public Sentence(Vector<Word> tokenizedInput)
	{
		this.tokenizedRealInput = tokenizedInput;
	}

}
