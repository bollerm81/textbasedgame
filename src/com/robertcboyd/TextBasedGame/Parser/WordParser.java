package com.robertcboyd.TextBasedGame.Parser;

import java.util.Vector;

import com.robertcboyd.TextBasedGame.Base.UI;
import com.robertcboyd.TextBasedGame.Commands.Command;
import com.robertcboyd.TextBasedGame.Parser.Word.PartOfSpeech;

public class WordParser {
	
	//TODO examine if the raw input ought to be stored somewhere in case of failures to parse
	/**
	 * Cleans up the raw user input and removes nonletter, nonnumber characters
	 *
	 * @param rawInput String containing the raw user input
	 *
	 * @return The revised, cleaned up input
	 */
	public static String parse(String rawInput)
	{
		return rawInput.toLowerCase().trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9]", "");
	}
	
	/**
	 * Utility method that tokenizes (splits) the user's input into individual Strings. This method also finds the part of speech for each String and stores the results of these calcuations in a Vector<Word>.
	 *
	 * @param cleanInput Sanitized user input without multiple spaces.
	 *
	 * @return A Vector<Word> containing the tokenized Strings converted to Words (data objects containing a String and a PartOfSpeech)
	 */
	public static Vector<Word> tokenize(String cleanInput)
	{
		//split based on whitespace
		String[] stringList = cleanInput.split("\\s+");
		
		//declare new Vector with words
		Vector<Word> wordList = new Vector<Word>();
		for (String s:stringList) {
			wordList.add(new Word(s, getPartOfSpeech(stringList, s)));
		}
		
		return wordList;
	}
	
	/**
	 * Attempts to discern the PartOfSpeech for a word (using context) as best as it can. This method always gives its best guess and will throw an exception if null or empty input is given.
	 *
	 * @param tokenizedCommand A nonnull String array containing the cleaned up, tokenized user input
	 * @param wordToCheck The String containing the word in question to determine the PartOfSpeech for
	 *
	 * @return The program's best guess at the PartOfSpeech for a String
	 */
	public static PartOfSpeech getPartOfSpeech(String[] tokenizedCommand, String wordToCheck)
	{
		//TODO fix this
		return PartOfSpeech.verb;
	}

	/**
	 * This method takes the user's raw input as a String and runs the appropriate Command. It does null checks to assure that the input is valid.
	 *
	 * @param rawInput The unmodified user input read from the console
	 */
	public static void processInput(String rawInput)
	{
		Sentence sentence = new Sentence(WordParser.tokenize(WordParser.parse(rawInput)));
		//null check to be sure that the sentence parsed and tokenized properly
		if(sentence != null) {
			Command c = bind(sentence);
			//null check to make sure that a bind was found
			if(c != null) {
				c.invoke(sentence);
			} else {
				UI.println("This sentence has triggered a critical error. Please contact the developer to debug.");
			}
		} else {
			UI.println("What utter gibberish!");
		}

	}

	/**
	 * Attempts to assign a meaning/bind a Command to the given Sentence
	 *
	 * @param sentence The sentence that needs binding to a Command
	 *
	 * @return The appropriate Command, or null if binding fails
	 */
	public static Command bind(Sentence sentence)
	{
	}
}
